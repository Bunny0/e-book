import 'package:flutter/material.dart';
import '../detail/detai.dart';
import '../data/manga.dart';

const PrimaryColor = const Color(0xFF050505);

class MangaPlus extends StatefulWidget {
  @override
  _MangaPlus createState() => _MangaPlus();
}

class _MangaPlus extends State<MangaPlus> {
  get details => Manga().details;
  List<Container> mangasss = [];
  get index => null;

  _listManga() async {
    for (var i = 0; i < details.length; i++) {
      final item = details[i];
      final String name = item["name"];
      final String image = item["image"];
      final String price = item["price"];
      final String synopsis = item["synopsis"];
      final String character = item["character"];

      mangasss.add(
          Container(
          child: Card(
              child: Column(
                children: <Widget>[
                  Hero(
                    tag: name,
                    child: new Material(
                      child: new InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => DetailBook(
                                      name: name,
                                      image: image,
                                      price: price,
                                      synopsis: synopsis,
                                      character: character)));
                        },

                        child: Image.asset(
                          "images/$image",
                          fit: BoxFit.cover,
                          width: 300.0,
                          height: 160.0,
                        ),
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      Expanded(child: Text(name, style: TextStyle(fontSize: 13.0, color: PrimaryColor),textAlign: TextAlign.center)),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                          child: Text('      price', style: TextStyle(fontSize: 13.0, color: PrimaryColor)),
                      ),
                      Container(
                        child: Text(' : ', style: TextStyle(fontSize: 13.0, color: PrimaryColor)),
                      ),
                      Expanded(
                        child: Container(
                            margin: EdgeInsets.zero,
                            width: 1.0,
                            height: 20.0,
                            alignment: Alignment.topRight,
                              child: OutlineButton(
                                child: Text(price,style: TextStyle(color: Colors.green)),
                                onPressed: () {},
                              ),
                        ),
                      ),
                    ],
                  )

                  ]
              )
          )
          )
      );
    }
  }

  @override
  void initState() {
    _listManga();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    final double itemWidth = (size.width / 2) - 5;
    return Scaffold(
      backgroundColor: Colors.greenAccent,
      body: GridView.count(
        crossAxisCount: 3,
        mainAxisSpacing: 25.0,
        childAspectRatio: (itemWidth / itemHeight),
        controller: new ScrollController(keepScrollOffset: false),
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        children: mangasss,
      ),
    );
  }
}
