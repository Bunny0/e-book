class LightNovel{
  var details=[{
    "name": "Tokidoki Bosotto Russia-go de Dereru Tonari no Aalya-san",
    "image": "light_novel/tokidoki_bosotto_roshia-go_de_dereru_tonari_no_aalya-san.jpg",
    "price":"260",
    "synopsis": " หา? พูดว่าไง? \nเปล่านี่? ก็แค่บอกว่า 【หมอนี่โง่จริงๆเลย】 แค่นั้นเอง \nเลิกด่าเป็นภาษารัสเซียได้มั้ย!? คุณอารยา สาวสวยผมเงินไม่เป็นสองรองใครที่นั่งข้างผมผุดยิ้มกระหยิ่ม \n…แต่ความจริงมันไม่ใช่อย่างนั้น ภาษารัสเซียเมื่อกี้ เธอพูดว่า 【สนใจฉันหน่อย】 ต่างหาก!",
    "character":"คุเซะ มาซาจิกะ (久世 政近) \nอาริสะ มิฮาอิลลอฟนา คุโจ (アリサ・ミハイロヴナ・九条) \nสุโอ ยูกิ (周防 有希|) \nมาริยา มิฮาอิลลอฟนา คุโจ (マリヤ・ミハイロヴナ・九条)",
    'isSelected': false
  },{
    "name": "Arifureta",
    "image": "light_novel/arifureta.jpg",
    "price": "272",
    "synopsis":"นากุโมะ ฮาจิเมะ เด็กที่มักจะโดนกลั่นแกล้ง ถูกเรียกตัวไปยังโลกใบอื่นพร้อมกับเหล่าเพื่อนร่วมชั้น ฮาจิเมะมีอาชีพแสนจะกระจอกอย่างนักเล่นแร่แปรธาตุ ตรงกันข้ามกับพวกเพื่อนร่วมชั้นที่แสดงศักยภาพในการสู้รบออกมาให้เห็นอย่างต่อเนื่อง ผู้อ่อนแอที่สุดแม้แต่ในโลกใบอื่นอย่างเขากลับถูกความชั่วร้ายของเพื่อนร่วมชั้นคนหนึ่งผลักให้ตกลงไปสู่ขุมนรกในเขาวงกต แล้วแบบนี้เขาจะรอดรึ!? \nท่ามกลางความสิ้นหวังเพราะหาทางหนีออกจากเขาวงกตไม่เจอ ฮาจิเมะก็ได้พบกับหนทางสู่การเป็นนักเล่นแร่แปรธาตุที่แข็งแกร่งที่สุด ก่อนโชคชะตาจะพาเขามาพบกับผีดูดเลือดยูเอะ \nฉันจะปกป้องยูเอะ ยูเอะจะปกป้องฉัน นั่นแหละคือความแข็งแกร่งที่แท้จริง มาโค่นล้มทุกสิ่งทุกอย่างให้หมดแล้วเป็นอันดับหนึ่งของโลกกันเถอะ \nขอเชิญรับชมฉากเปิดของโลกแฟนตาซี! การพบกันของผีดูดเลือดผู้มาจากส่วนลึกที่สุดของโลกกับเด็กหนุ่มผู้กลับมาจากขุมนรก กำลังจะสร้างตำนาน สุดแกร่ง ให้โลกตะลึง",
    "character":"Hajime \nYue \nShea \nTio \nKaori \nShizuku",
    'isSelected': false
  },{
    "name": "Gimai Seikatsu",
    "image": "light_novel/gimai_seikatsu.jpg",
    "price": "240",
    "synopsis":"ชีวิตใหม่กับน้องสาวป้ายแดง....เรื่องราวเริ่มขึ้นเมื่อพ่อของยูตะและแม่ของซากิแต่งงานกันใหม่ ทำให้ทั้งคู่ที่เป็นเพื่อนร่วมชั้นเรียนกันเปลี่ยนสถานะมาเป็นพี่ชายกับน้องสาวในทันที",
    "character":"YUUTA ASAMURA \nSAKI AYASE \nTAICHI ASAMURA \nAKIKO AYASE \nTOMOKAZU MARU \nMAAYA NARASAKA \nSHIORI YOMIURI",
    'isSelected': false
  },{
    "name": "Hen suki",
    "image": "light_novel/hensuki.jpg",
    "price": "235",
    "synopsis":"ผม คิริว เคกิ \nจู่ๆก็ได้รับจดหมายรักฉบับหนึ่งที่ไม่ระบุชื่อผู้ส่ง \nหากพิจารณาจากสถานการณ์ บุคคลที่มีความเป็นไปได้ \nว่าจะเป็นเจ้าของจดหมาย \nคือหนึ่งในบรรดาหญิงสาวที่มีส่วนร่วม \nในการทำความสะอาดห้องชมรมเขียนพู่กัน",
    "character":"Keiki Kiryuu \nSayuki Tokihara \nYuika Koga \nMizuha Kiryuu \nMao Nanjou \nShouma Akiyama",
    'isSelected': false
  },{
    "name": "Loli 2",
    "image": "light_novel/loli2.jpg",
    "price": "212",
    "synopsis":"อาจารย์คะ ช่วยรับหนูเป็นลูกศิษย์ด้วยนะคะ  \nมิยุจัง เด็กสาวได้รับรู้ความลับของ ชาลี เด็กหนุ่มว่า ตัวจริงของคือท่านอาจารย์ 007H นักเขียนนิยายลามกชื่อดังที่เธอคลั่งไคล้ เด็กสาวผู้อยากจะเขียนนิยายลามกแบบนี้บ้างจึงมาขอฝากตัวเป็นลูกศิษย์ของชาลี พร้อมกับเรื่องวุ่น ๆ ของเด็กหนุ่มที่อยากไล่เธอออกไปก่อนที่ชีวิตส่วนตัวของตัวเองจะพินาศไปในพริบตา ทว่า นานวันเข้าความสัมพันธ์ของอาจารย์หนุ่มกับลูกศิษย์ตัวน้อยก็เริ่มแน่นแฟ้นมากขึ้น \nนิยายลามกเรื่องใหม่ครั้งนี้ชาลีจะเขียนเสร็จก่อนหรือเข้าไปนอนในดาวหมีก่อนกันนะ ?",
    "character":"มิยุ \nชาลี \nอรภา \nคาริสา \nโมโม่",
    'isSelected': false
  },{
    "name": "No Game No Life",
    "image": "light_novel/no_game_no_life.jpg",
    "price": "150",
    "synopsis":"่โซระและชิโระเป็นสองพี่น้องที่เล่นเกมเก่งระดับเทพ แต่ก็เป็นนีทและฮิคิโคโมริด้วย วันหนึ่งพวกเขาถูกเชิญไปที่โลกต่างมิติ ที่นั่นเป็นโลกที่ตัดสินทุกอย่างด้วยเกม ไม่ว่าจะเป็นอาหาร เงินทอง หรือแม้แต่เขตแดนประเทศ! \nแม้จะเป็นคนไม่ได้เรื่องในความเป็นจริง แต่สองพี่น้องจะช่วยเผ่ามนุษย์ในต่างโลกให้รอดพ้นจากวิกฤติได้หรือไม่? \nเอาล่ะ มาเริ่มเกมกันเถอะ!",
    "character":"Sora \nShiro \nStephanice Dola \nChlammy Zell \nFiel Nirvalen \nJibril \nIzuna Hatsue \nTet",
    'isSelected': false
  },{
    "name": "Oshiego ni Kyouhaku Sareru no wa Hanzai desu Ka",
    "image": "light_novel/oshiego_ni_kyouhaku_sareru_no_wa_hanzai_desu_ka.jpg",
    "price": "270",
    "synopsis":"เท็นจิน ครูกวดวิชาวัย 27 ปี ทำงานอยู่ที่โรงเรียนกวดวิชาเตรียมสอบเข้ามัธยมต้นและปลาย มีความเชี่ยวชาญด้านการชี้แนะเด็กนักเรียนชั้นประถมและมัธยมต้น แต่ไม่มีความรู้สึกใดๆต่อเด็กทั้งสิ้น จะวันนี้หรือพรุ่งนี้ก็ยังทำงานไปเรื่อยๆด้วยความรู้สึกเฉยชา โดยที่ระหว่างนั้นก็โดนรอบข้างสั่งสอนให้เห็นสัจธรรมของโลกบ้าง หรือบางทีก็เป็นฝ่ายสั่งสอนนักศึกษาสาวเพื่อนร่วมงานบ้าง ทว่า— \nหนูชอบ ชอบ ชอบอาจารย์ที่สุดเลย... \nวันหนึ่งเขากลับถูกยัยปีศาจสาวตัวแสบของฝ่ายมัธยมต้น (นักเรียนหญิง ม.ต้น อายุ 14 ปี) พบเห็นตอนที่โดนเด็กนักเรียนหญิงชั้น ป.5 ของห้องที่ตัวเองดูแลอยู่เข้ามาคลอเคลียเสียอย่างนั้น... \nช่วยสอนบทเรียนส่วนตัวตอนกลางคืนให้หนูทีสิคะ—นะ อาจารย์โลลิคอนชั้นหนึ่ง \nเมื่อโดนรีเควสต์มาเช่นนี้ชั่วโมงเรียนภาคบังคับนอกหลักสูตรจึงเริ่มต้นขึ้นโดยไม่ได้ตั้งใจ!? พบกับเรื่องราวความรักต่างวัยสุดอลวนที่ยากจะพ้นคุก จากปลายปากกาของ โซ ซางาระ ผู้แต่งองค์ชายจิตป่วนกับน้องเหมียวยิ้มยาก! ความจริง ต้องห้ามของนักเรียนประถมและมัธยมต้นที่ไม่เคยมีใครรู้มีอยู่ในเล่มนี้—!?",
    "character":"Null",
    'isSelected': false
  },{
    "name": "Otonari notenshi-sama ni Itsu no_ma ni ka dame ningen ni sareteita ken",
    "image": "light_novel/otonari_notenshi-sama_ni_itsu_no_ma_ni_ka_dame_ningen_ni_sareteita_ken.jpg",
    "price": "235",
    "synopsis":"\n…เธอกำลังทำอะไรอยู่งั้นหรอ? \nท่ามกลางสายฝน ชีนะ มาฮิรุ เธอกำลังนั่งอยู่คนเดียวบนชิงช้าในสวนสาธารณะ นั่นเป็นครั้งแรกที่อามาเนะ ฟูจิมิยะได้พูดคุยกับเธอ \nอามาเนะได้เริ่มออกมาอาศัยอยู่ตัวคนเดียวตั้งแต่เริ่มเข้ามัธยมปลายปี 1 และผู้ที่อาศัยอยู่ในอพาทเม้นท์ติดห้องติดกันทางด้านขวาของเขาก็คือนางฟ้า",
    "character":"Shiina, Mahiru \nFujimiya, Amane \nShirakawa, Chitose \nAkazawa, Itsuki \nFujimiya, Shihoko \nKadowaki, Yuuta",
    'isSelected': false
  },{
    "name": "Seishun buta yarou Wa bunny girl senpai no yume wo minai",
    "image": "light_novel/Seishun_buta_yarou_wa_bunny_girl_senpai_no_yume_wo_minai.jpg",
    "price": "279",
    "synopsis":"รู้ทั้งรู้ว่าบันนี่เกิร์ลไม่มีทางปรากฏตัวในหอสมุดได้ \nแต่อาสุซากาวะ ซาคุตะ ก็ได้พบกับบันนี่เกิร์ลตัวเป็นๆ \nมิหนำซ้ำเธอยังเป็นรุ่นพี่สุดสวย ซากุระจิมะ มาอิ \nอดีตดาราที่พักงานในวงการบันเทิงไปแล้วด้วยอีกต่างหาก",
    "character":"Sakuta Azusagawa \nMai Sakurajima \nTomoe Koga \nRio Futaba \nNodoka Toyohama \nKaede Azusagawa \nShoko Makinohara",
    'isSelected': false
  },{
    "name": "Tokidoki bosotto roshia-go de dereru tonari no aalya-san2",
    "image": "light_novel/tokidoki_bosotto_roshia-go_de_dereru_tonari_no_aalya-san2.jpg",
    "price": "265",
    "synopsis":"ชะ...ชอบ? ชอบเหรอ เอ๊ะ? ไม่ใช่สักหน่อย ไม่ใช่น้าาาา～! \nทำมาเป็น～ ‘ฉันจะเป็นกำลังให้เอง’ โอ๊ย～เรานี่มันน่าขนลุก สะเหล่อ! \nจากเหตุการณ์ลับที่รู้กันสองคนซึ่งเกิดขึ้นในสวนโรงเรียนยามอาทิตย์อัสดง \nทั้งอาเรียและมาซาจิกะต่างกลุ้มใจกันทั้งคู่ \nแต่ทั้งสองก็ได้สัญญาแล้วว่าจะจับคู่กันเพื่อลงชิงชัยในสนามเลือกตั้งประธานนักเรียน",
    "character":"คุเซะ มาซาจิกะ (久世 政近) \nอาริสะ มิฮาอิลลอฟนา คุโจ (アリサ・ミハイロヴナ・九条) \nสุโอ ยูกิ (周防 有希|) \nมาริยา มิฮาอิลลอฟนา คุโจ (マリヤ・ミハイロヴナ・九条)",
    'isSelected': false
  },{
    "name": "Yatarato sasshi no Ii ore wa",
    "image": "light_novel/yatarato_sasshi_no_Ii_ore_wa.jpg",
    "price": "230",
    "synopsis":"Yatarato Sasshi no Ii Ore wa, Dokuzetsu Kuudere Bishoujo \nno Chiisana Dere mo Minogasazu ni Guigui Iku , Relentlessly Approaching the \nPoison-Tongued and Indifferent Beauty to Tickle the Cutesy Reactions out of \nher , やたらと察しのいい俺は、毒舌クーデレ美少女の小さなデレも見 \n逃さずにぐいぐいいく",
    "character":"Null",
    'isSelected': false
  },
    {
      "name": "Rascal Does not Dream of a sister home alone",
      "image": "light_novel/rascal_does_not_dream_of_a_sister_home_alone.jpg",
      "price": "280",
      "synopsis":"ซาคุตะ ลำบากใจตั้งแต่เช้าในวันที่การสอบกลางภาคใกล้จะมาถึง \nเมื่อโชโกะผู้เป็นรักแรกส่งจดหมายมาว่า อยากเจอ \nและเขาก็ยังไม่ได้บอกเรื่องนั้นกับ มาอิ \nหรือนี่จะเป็นลางบอกเหตุว่าความวุ่นวายกำลังจะมาเยือนอีกแล้ว!?",
      "character":"Sakuta Azusagawa \nMai Sakurajima \nTomoe Koga \nRio Futaba \nNodoka Toyohama \nKaede Azusagawa \nShoko Makinohara",
      'isSelected': false
    }
  ];
}