class Manga{
  var details = [{
    "name":"No Game No Life",
    "image":"manga/no_game_no_life.jpg",
    "price":"80",
    "synopsis":" โซระและชิโระเป็นสองพี่น้องที่เล่นเกมเก่งระดับเทพ แต่ก็เป็นนีทและฮิคิโคโมริด้วย วันหนึ่งพวกเขาถูกเชิญไปที่โลกต่างมิติ ที่นั่นเป็นโลกที่ตัดสินทุกอย่างด้วยเกม ไม่ว่าจะเป็นอาหาร เงินทอง หรือแม้แต่เขตแดนประเทศ! \nแม้จะเป็นคนไม่ได้เรื่องในความเป็นจริง แต่สองพี่น้องจะช่วยเผ่ามนุษย์ในต่างโลกให้รอดพ้นจากวิกฤติได้หรือไม่? \nเอาล่ะ มาเริ่มเกมกันเถอะ!",
    "character":" Sora \nShiro \nStephanice Dola \nChlammy Zell \nFiel Nirvalen \nJibril \nIzuna Hatsue \nTet",
    'isSelected': false
  },{
    "name":"Horimiya",
    "image":"manga/horimiya.jpg",
    "price":"90",
    "synopsis":" Kyouko Hori seems like an average teenage girl, but she has a \ndifferent side outside of school that she wants no one else to ever \nfind out about. Brought up in a household where both her parents \nare absent from working, she needs to take care of the house and \nher little brother, unable to lead a normal social life. Likewise, there \nis her classmate Izumi Miyamura, who has a different side out of \nschool that remains hidden from others. When the two meet \nunexpectedly, they discover each others' secrets and develop a \nspecial friendship.",
    "character":" KYOUKO HORI \nIZUMI MIYAMURA \nYUKI YOSHIKAWA \nTOORU ISHIKAWA \nKAKERU SENGOKU \nREMI AYASAKI \nSAKURA KOUNO \nSYU IURA \nAKANE YANAGI",
    'isSelected': false
  },{
    "name":"Isekai Nonbiri Nouka",
    "image":"manga/isekai_nonbiri_nouka.jpg",
    "price":"85",
    "synopsis":" Farming Life in Another World, better known as Isekai Nonbiri Nouka \nhas large number of characters whom our hero ends up being \nsurrounded by as they seem all end up on his ever growing farm.",
    "character":" Hiraku Machio \nKuro & Yuki \nRu Rurushi \nAlfred \nTia \nTissele \nZabuton  \nRia ",
    'isSelected': false
  },{
    "name":"Gobiln Slayer",
    "image":"manga/gobiln_slayer.jpg",
    "price":"80",
    "synopsis":" oblin Slayer (ゴブリンスレイヤー, Goburin Sureiyā) is the series' \ntitular main character. He is an experienced silver-ranked \nadventurer who mainly concerns himself with hunting goblins.",
    "character":" Goburin Sureiyā",
    'isSelected': false
  },{
    "name":"Kimitsu no Yaba",
    "image":"manga/kimetsu_no_yaiba.jpg",
    "price":"70",
    "synopsis":" ในยุคไทโชของญี่ปุ่น ทันจิโร่ คามาโดะเป็นเด็กใจดีที่หาเลี้ยงชีพด้วยการขายถ่าน อย่างไรก็ตามในยุคไทโชของญี่ปุ่น ทันจิโร่ คามาโดะเป็นเด็กใจดีที่หาเลี้ยงชีพด้วยการขายถ่าน อย่างไรก็ตาม ชีวิตที่สงบสุขของเขาต้องพังทลายเมื่อเหล่าอสูรสังหารทั้งครอบครัวของเขา เนซึโกะ \nน้องสาวคนเล็กของเขาเป็นผู้รอดชีวิตเพียงคนเดียว แต่เธอได้กลายเป็นอสูรไปแล้ว! ทันจิโร่ออกเดินทางสุดอันตรายเพื่อหาวิธีคืนน้องสาวของเขาให้เป็นปกติและฆ่าอสูรที่ทำลายชีวิต \nของเขาวิตของเขา",
    "character":" ทันจิโร่ คามาโดะ \nเนซึโกะ คามาโดะ \nเซนอิทสึ อะกะสึมะ  \nอิโนะสุเกะ ฮาชิบิระ \nคานาโอะ สึยุริ",
    'isSelected': false
  },{
    "name":"Komi san wa Komyushou desu",
    "image":"manga/komi_san_wa_komyushou_desu.jpg",
    "price":"90",
    "synopsis":" Komi-san is the beautiful and admirable girl that no one can take their  \neyes off of. Almost the whole school sees her as the cold beauty  \nthat's out of their league, but Tadano Hitohito knows the truth: she's  \njust really bad at communicating with others. \nKomi-san, who wishes to fix this bad habit of hers, tries to improve  \nherself with the help of Tadano-kun.",
    "character":" Shousuke \nManbagi \nYamai \nShouko \nHitohito \nOsana \nOnemine \nKimitani \nNaruseshisuto",
    'isSelected': false
  },{
    "name":"Kubo san dosen't leave me be",
    "image":"manga/kubo_san_doesn't_leave_me_be.jpg",
    "price":"90",
    "synopsis":" The series follows Shiraishi, who has no presence, which causes him \nto be overlooked by almost everyone. However, his classmate Nagisa \nKubo has no problem finding him, and declares herself his heroine.  \nThroughout the series, Nagisa plays with Shiraishi to try and get her  \nfeelings through to him.",
    "character":" Seita \nhiraihi \nNagisa Kubo \nAkina Kubo ",
    'isSelected': false
  },{
    "name":"Lv999 no Murabito",
    "image":"manga/lv999_no_murabito.jpg",
    "price":"75",
    "synopsis":" As a child Kagami chose to have a neutral role because his father was \nkilled by monsters and his mother was killed by humans. He decided \nto become stronger and killed monsters for years while leveling up, \nafter some time he began to question the reason why monsters drop \ncurrency which is used by humans and presumably stumbled upon an  \nanswer.",
    "character":" Kagami \nAlice Balnesio \nDemon King \nKan-Nazuki Kururu \nRex",
    'isSelected': false
  },{
    "name":"Parallel Paradise",
    "image":"manga/parallel_paradise.jpg",
    "price":"75",
    "synopsis":" ทาดะ โยตะ นักเรียนมัธยมปลายได้หลุดเข้าไปในป่าต่างโลก และได้พบเข้ากับรูมี่ซึ่งเป็นอัศวินสาวสวย \nที่นี่เป็นโลกที่มีเพียงแต่ผู้หญิงเท่านั้น \nโยตะได้รับมอบหมายจากร่างจำแลงเทพให้มีเพศสัมพันธ์กับรูมี่ที่เกิดอาการขึ้นอย่างสุดขั้วเพียงแค่ถูกสัมผัส...!?",
    "character":" Youta Tada \nLilia \nLumi \nGenius \nMona and Lisa \nHaru \nMisaki \nMomo \nYuuri \nNagomi \nKia \nPeko",
    'isSelected': false
  },{
    "name":"Solo Leveling",
    "image":"manga/solo_leveling.jpg",
    "price":"80",
    "synopsis":" ซองจินอู ฮันเตอร์ระดับ E ผู้ไร้ความสามารถตลอดกาล \nเดิมพันกับความตายที่อยู่ตรงหน้าในดันเจี้ยนประหลาด \nทว่าวิกฤตมักมาพร้อมโอกาสเสมอ! \n[คุณได้รับสิทธิ์เป็นผู้เล่น] \nผู้เล่นเหรอ นี่ฉันอัพเลเวลได้ด้วยเหรอ \nเมื่อพลังในตัวของซองจินอูตื่นจากการหลับใหล \nแถมยังพร้อมสรรพด้วยระบบซัพพอร์ตและการอัพเลเวลสูงขึ้นไป \nการป่าวประกาศอิสรภาพแก่โลกทั้งใบก็ไม่ใช่เรื่องไกลเกินเอื้อม! \nอาวุธสุดกากแห่งมวลมนุษยชาติ หนึ่งเดียวคนนี้จะทำให้ฮันเตอร์ทั่วทั้งโลกตกตะลึง!? \nผลงานจากปลายปากกาของชู่กง ผู้ประพันธ์ I Saw The King \nจะเผยแก่นแท้แห่งเรดให้เป็นที่ประจักษ์!",
    "character":" Sung Jinwoo \nThomas Andre \nLiu Zhigang \nChristopher Reed \nSung Il-Hwan \nGo Gunhee \nGo Gunhee \nGoto Ryuji \nLennart Niermann \nHwang Dongsoo \nJay Mills \nYuri Orloff \nReiji Sugimoto \nCha Hae-In",
    'isSelected': false
  },{
    "name":"Tsuki ga Michibiku isekai douchuu",
    "image":"manga/tsuki_ga_michibiku_isekai_douchuu.jpg",
    "price":"90",
    "synopsis":" นั้นแหละ",
    "character":" Makoto Misumi \nTomoe \nHibiki Otonashi \nMio \nTamaki",
    'isSelected': false
  },{
    "name":"Yo-Jo life!",
    "image":"manga/yo-jo_life!.jpg",
    "price":"60",
    "synopsis":" Akihiko Shimura, 20 years old (male). When I woke up in the morning, I was a little blonde loli.",
    "character":" Yo-Jo",
    'isSelected': false
  }
  ];
}