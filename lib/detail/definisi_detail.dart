import 'package:flutter/material.dart';

class DefinisiEbook extends StatelessWidget {
  final String synopsis;

  const DefinisiEbook({Key? key, required this.synopsis}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: new EdgeInsets.all(10.0),
      child: new Card(
        child: new Padding(
            padding: const EdgeInsets.all(10.0),
            child: new Text(synopsis,
                style: new TextStyle(fontSize: 18.0),
                textAlign: TextAlign.justify)),
      ),
    );
  }
}
