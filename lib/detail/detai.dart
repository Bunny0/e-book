import 'package:flutter/material.dart';
import 'definisi_detail.dart';
import 'story_details.dart';

class DetailBook extends StatelessWidget {
  final String name;
  final String image;
  final String price;
  final String synopsis;
  final String character;

  const DetailBook(
      {Key? key, required this.name, required this.image, required this.synopsis, required this.character, required this.price})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("$name"),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, false),
        ),
      ),
      body: new ListView(
        children: <Widget>[
          new Container(
            child: new Hero(
              tag: name,
              child: new Material(
                child: new InkWell(
                  child: new Image.asset("images/$image", fit: BoxFit.cover),
                ),
              ),
            ),
          ),
          new DefinisiEbook(synopsis: synopsis),
          new Ebookss(character: character, ac: '',)
        ],
      ),
    );
  }
}
