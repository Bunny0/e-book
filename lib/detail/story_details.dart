import 'package:flutter/material.dart';

class Ebookss extends StatelessWidget {
  final String character;
  final String ac;


  const Ebookss({Key? key, required this.character, required this.ac}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: new EdgeInsets.all(10.0),
      child: new Card(
        child: new Padding(
          padding: const EdgeInsets.all(10.0),
          child: new Text(
            "Character - character \n\n$character",
            style: new TextStyle(fontSize: 18.0),
            textAlign: TextAlign.left,
          ),
        ),
      ),
    );
  }
}
