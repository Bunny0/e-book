import 'package:flutter/material.dart';
import '../data/light_novel.dart';
import '../data/manga.dart';

class ToBuy extends StatefulWidget {
  @override
  _ToBuy createState() => _ToBuy();
}

class _ToBuy extends State<ToBuy> {
  int selectedIndex = -1;
  List listBook = Manga().details+LightNovel().details;
  int count = Manga().details.length+LightNovel().details.length;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('    price                        Selete for Buy', style: TextStyle(fontSize: 15.0,color: Colors.black),),
          // centerTitle: true,
          toolbarHeight: 40.0,

        ),
        body: SafeArea(
            child: ListView.builder(
              itemCount: count,
              itemBuilder: (BuildContext ctx, index) {
                return Card(
                    key: ValueKey(listBook[index]['name']),
                    margin: const EdgeInsets.all(10),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    // The color depends on this is selected or not
                    color: listBook[index]['isSelected'] == true
                        ? Colors.amber
                        : Colors.white,
                    child: ListTile(
                      onTap: () {
                        // if this item isn't selected yet, "isSelected": false -> true
                        // If this item already is selected: "isSelected": true -> false
                        setState(() {
                          listBook[index]['isSelected'] = !listBook[index]['isSelected'];
                        });
                      },
                      leading: CircleAvatar(
                          // backgroundColor: Colors.blue,
                          child: Text(listBook[index]['price'.toString()]),

                      ),

                      title: Text(listBook[index]['name']),
                    ),
                );
              },
            )
        ), bottomNavigationBar: Material(
            color: const Color(0xffff8906),
            child: InkWell(
              onTap: () {
                _showMyDialog();
                print(count);
            },
            child: const SizedBox(
              height: kToolbarHeight,
              width: double.infinity,
              child: Center(
                 child: Text('¥Buy', style: TextStyle(fontWeight: FontWeight.bold,),
    ),
    ),
    ),
    ),
    ),
    );
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(

          title: Text('Order Confirmation', textAlign: TextAlign.center),

          content: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Text('Please look at the product list carefully. \nBecause the transaction has been confirmed, it cannot be cancelled.', textAlign: TextAlign.center,),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Confirm'),
              onPressed: () {
                Navigator.of(context).pop();
                setState(() {
                  for(var i=0;i<listBook.length;i++){
                    listBook[i]['isSelected'] = false;
                  }
                });
                print('OnTap ');
              },
            ),
            TextButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _pullRefresh() async {
    List listBook = Manga().details+LightNovel().details;
    setState(() {
      listBook = listBook;
    });
  }
}


