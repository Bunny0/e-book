import 'package:bs/user_profile/user.dart';
import 'package:flutter/material.dart';
import '_manga/manga_book.dart';
import '_light_novel/light_novel_book.dart';
import 'list_buy.dart';
import 'shopping_book/Cardview.dart';

const PrimaryColor = const Color(0xff00f279);

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: "BookStore",
      theme: ThemeData(primaryColor: PrimaryColor),
      home: MyApp(),
    );
  }
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State {
  final List<Widget> _children = [
    MangaPlus(),
    LightNovelPlus(),
    ToBuy(),
    OrderPage(),
    User() 
  ];
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("E-Book"), centerTitle: true),
      body: Center(
        child: _children.elementAt(_currentIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.book),
            label: "Manga",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book),
            label: "Light Novel",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_basket),
            label: "To Buy",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book),
            label: "Shopping",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.verified_user),
            label: "User",
          )
        ],
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
